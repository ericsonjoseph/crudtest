package crudtest;

/**
 *
 * @author ericsonjoseph@comtor.net
 */
public class CRUDTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        StringBuilder sb = new StringBuilder();

        sb.append("<html>\n"
                + "<head>\n"
                + "  <meta name=\"generator\" content=\n"
                + "  \"HTML Tidy for Linux (vers 25 March 2009), see www.w3.org\">\n"
                + "  <meta http-equiv=\"content-type\" content=\n"
                + "  \"text/html; charset=us-ascii\">\n"
                + "\n"
                + "  <title>DataTables Bootstrap 3 example</title>\n"
                + "  <link rel=\"stylesheet\" href=\n"
                + "  \"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"\n"
                + "  type=\"text/css\">\n"
                + "  <script src=\n"
                + "  \"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"\n"
                + "  type=\"text/javascript\">\n"
                + "</script>\n"
                + "  <script src=\n"
                + "  \"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"\n"
                + "  type=\"text/javascript\">\n"
                + "</script>\n"
                + "  <link rel=\"stylesheet\" type=\"text/css\" href=\n"
                + "  \"https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.css\">\n"
                + "  <script type=\"text/javascript\" src=\n"
                + "  \"https://cdn.datatables.net/v/bs/dt-1.10.16/datatables.min.js\">\n"
                + "</script>\n"
                + "  <script type=\"text/javascript\" charset=\"utf-8\">\n"
                + "                    $(document).ready(function() {\n"
                + "                                $('#example').DataTable();\n"
                + "                        } );\n"
                + "  </script>\n"
                + "</head>\n"
                + "\n"
                + "<body>\n"
                + "  <div class=\"container\">\n"
                + "    <table id=\"example\" class=\"display\" cellspacing=\"0\" width=\n"
                + "    \"100%\">\n"
                + "      <thead>\n"
                + "        <tr>\n"
                + "          <th>Name</th>\n"
                + "\n"
                + "          <th>Position</th>\n"
                + "\n"
                + "          <th>Office</th>\n"
                + "\n"
                + "          <th>Salary</th>\n"
                + "        </tr>\n"
                + "      </thead>\n"
                + "\n"
                + "      <tbody>");

        for (int i = 0; i < 100000; i++) {
            sb.append("<tr>\n"
                    + "          <td>" + "NAME " + i + "</td>\n"
                    + "\n"
                    + "          <td>" + "REGION " + i + "</td>\n"
                    + "\n"
                    + "          <td>" + "CITY " + i + "</td>\n"
                    + "\n"
                    + "          <td>" + (i * 100) + "</td>\n"
                    + "        </tr>");
        }

        sb.append("      </tbody>\n"
                + "    </table>\n"
                + "  </div><script type=\"text/javascript\">\n"
                + "    // For demo to fit into DataTables site builder...\n"
                + "        $('#example')\n"
                + "                .removeClass( 'display' )\n"
                + "                .addClass('table table-striped table-bordered');\n"
                + "  </script>\n"
                + "</body>\n"
                + "</html>");

        System.out.println(sb.toString());
        
        

    }

}
